

// 3. Fetch() all the TODO LIST from JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.table(json));

// 4. Create an array using the arrayName.map() method to return just the title of every item and print the result in the console.
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(toDoArray => {
   let arrayToDo = toDoArray.map((elem) => {
         return(elem.title)
  }); console.log(arrayToDo)
});

// 5. Create a fetch() request using the GET method that will retrieve a single to do list item from JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {
    let result = {
        completed: json.completed,
        id: json.id,
        title: json.title,
        userId: json.userId
    };
    console.log(result)
});

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {
    console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`)
});

// 7. Create a fetch request using the POST method that will create a to do list using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		completed: false, 
		id: 201,
        title: 'Created To Do List',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		completed: true, 
		id: 205,
        title: 'Fetch PUT Request Example',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));


/* 9. Update a to do list item by changing the data structure to contain the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID
*/ 

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		dateCompleted: "Pending", 
        description: "To update my to do list with a different data structure", 
		id: 1,
        status: "Pending",
        title: "Updated To Do List Item",
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// 10. Create a fetch request using the PATCH Method that will update a to do list item using the JSON Placeholder API. 
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {'Content-type': 'application/json'},

	body: JSON.stringify({
		completed: true, 
        title: "This is a PATCH method example"
	}),

})
.then((response) => response.json())
.then((json) => console.log(json));

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		completed: false, 
        dateCompleted: "07/09/21", 
		id: 1,
        status: "Complete",
        title: "delectus aut autem",
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// 12. Create a fetch request using the DELETE method that will delete an item using JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE',
})
.then((response) => response.json())
.then(data => console.log(data));

/* 13. Create a request via Postman to retrieve all the to do list items.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list items.
*/

/*14. Create a request via Postman to retrieve an individual to do list item.
a. GET HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as get all to do list items.
*/ 

/*15. Create a request via Postman to create a to do list item.
a. POST HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list items.
*/

/*16. Create a request via Postman to update a to do list item.
a. PUT HTTP method
b. https://jsonplaceholder.typicode.com/todos URI endpoint
c. Save this request as get all to do list item PUT
d. Update the to do list item to mirror the data structure used in the PUT fetch request.
*/

/*17. Create a request via Postman to update a to do list item.
a. PATCH HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as create to do list item
d. Update the to do list item to mirror the data structure of the PATCH fetch request.
*/

/*18. Create a request via Postman to delete a to do list item.
a. DELETE HTTP method
b. https://jsonplaceholder.typicode.com/todos/1 URI endpoint
c. Save this request as delete to do list item
*/







